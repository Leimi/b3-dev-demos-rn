import React, {useState} from 'react';
import {StatusBar, Text, View, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import SimpleComponents from "./demos/1-SimpleComponents/SimpleComponents";
import HomemadeComponents from "./demos/2-HomemadeComponents/HomemadeComponents";
import EventfulComponents from "./demos/3-EventfulComponents/EventfulComponents";
import StatefulComponents from "./demos/4-StatefulComponents/StatefulComponents";
import ControlledComponents from "./demos/5-ControlledComponents/ControlledComponents";
import TodoList from "./demos/6-TodoList/TodoList";
import EffectCounterComponent from "./demos/7-ComponentsWithEffects/1-Counter";
import EffectFetchComponent from "./demos/7-ComponentsWithEffects/2-Fetch";
import MDSNews from "./demos/8-MDSNews/MDSNews";

const DEMOS = {
	"1 - Découverte d'un composant": SimpleComponents,
	"2 - Exercice : découper un composant": HomemadeComponents,
	"3 - Événements": EventfulComponents,
	"4 - État": StatefulComponents,
	"5 - Composants contrôlés": ControlledComponents,
	"6 - Exercice : TodoList": TodoList,
	"7 - Effets de bord (compteur)": EffectCounterComponent,
	"7 - Effets de bord (API)": EffectFetchComponent,
	"8 - MDSNews": MDSNews,
}

export default function App() {
	const [demoLabel, setDemo] = useState(null);
	const DemoComponent = demoLabel ? DEMOS[demoLabel] : null
	return (
		<SafeAreaView>
			<StatusBar />
			<View>
				<Picker
					selectedValue={demoLabel}
					onValueChange={label => {
						setDemo(label)
					}}
				>
					<Picker.Item label="Choisir une démo" value="" />
					{ Object.keys(DEMOS).map(label =>
						<Picker.Item key={label} label={label} value={label} />
					)}
				</Picker>
			</View>
      		<View style={styles.scrollView}>
				{/* on passe en enfant du composant voulu son label.
				c'est un peu bizarre, mais c'est pour avoir directement un exemple
				de composant avec des `children` derrière dans la démo 1.
				En tant normal, il n'y a pas forcément d'intérêt à faire ça comme ça. */}
				{ DemoComponent && <DemoComponent>{demoLabel}</DemoComponent> }
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	scrollView: {
		padding: 10,
	}
})
	