// avec JSX :
const element = (
	<ul className="TaskList">
		<li>
			<div className="TaskItem">
				<input type="checkbox" id="task-1-done" className="TaskItem-checkbox"/>
				<label htmlFor="task-1-done" className="TaskItem-title">Formation React (element, avec JSX)</label>
				<button type="button" className="TaskItem-deleteButton">Supprimer</button>
			</div>
		</li>
	</ul>
);

ReactDOM.render(element, document.querySelector('#main'));

// sans JSX :
const element = React.createElement("ul", {className: "TaskList"},
	React.createElement("li", null,
		React.createElement("div", {className: "TaskItem"},
			React.createElement("input", {type: "checkbox", id: "task-1-done", className: "TaskItem-checkbox"}),
			React.createElement("label", {htmlFor: "task-1-done", className: "TaskItem-title"}, "Formation Initiation JS (element, sans JSX)"),
			React.createElement("button", {type: "button", className: "TaskItem-deleteButton"}, "Supprimer")
		)
	)
);

ReactDOM.render(element, document.querySelector('#main'));