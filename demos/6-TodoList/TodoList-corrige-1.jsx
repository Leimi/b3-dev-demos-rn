import React, { useState } from 'react';
import { View, Text, TextInput, Button, TouchableOpacity } from 'react-native';

/**
 * Exercice
 * 
 * Créer une vue qui va gérer :
 * 
 * - l'affichage d'une liste,
 * - un ajout d'éléments dans cette liste via un champ texte,
 * - la suppression d'élément de cette liste en cliquant sur un élément de la liste
 * 
 * pour l'instant, pas grave si on arrive pas à scroller correctement dans notre liste
 * une fois qu'elle est longue... on veut gérer une toute petite liste qui tient sur
 * l'écran comme ça
 */
const TodoList = () => {
	const [inputValue, setInputValue] = useState('')
	const [items, setItems] = useState([])
	const addItem = () => {
		if (!inputValue) {
			return
		}
		setItems(previousState => ([
			...previousState,
			inputValue
		]))
		setInputValue('')
	}
	const removeItem = (i) => {
		setItems(previousState => previousState.filter((item, index) => index !== i))
	}
	return (
		<View>
			<Text>Ajouter un élément :</Text>
			<TextInput
				onChangeText={text => setInputValue(text)}
				value={inputValue}
				style={{
					borderColor: "#ddd",
					borderWidth: 1,
					borderRadius: 4,
					paddingHorizontal: 12,
					paddingVertical: 3,
					marginTop: 6,
					marginBottom: 12
				}}
			/>
			<Button onPress={addItem} title="Ajouter" />

			<Text
				style={{
					fontWeight: 'bold',
					marginTop: 12,
					fontSize: 24,
					marginBottom: 12
				}}
				accessibilityRole="header"
			>
				Liste
			</Text>

			{items.map((item, index) => (
				<Text
					key={`${index}${item}`}
					onPress={() => removeItem(index)}
				>
					• {item}
				</Text>
			))}
		</View>
	);
}


export default TodoList
