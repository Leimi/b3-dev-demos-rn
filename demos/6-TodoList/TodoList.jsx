import React, { useEffect, useState } from 'react';
import { Text, View, Button, TextInput } from 'react-native';

/**
 * Exercice
 *
 * Créer une vue qui va gérer :
 *
 * - l'affichage d'une liste,
 * - un ajout d'éléments dans cette liste via un champ texte,
 * - la suppression d'élément de cette liste en cliquant sur un élément de la liste
 *
 * pour l'instant, pas grave si on arrive pas à scroller correctement dans notre liste
 * une fois qu'elle est longue... on veut gérer une toute petite liste qui tient sur
 * l'écran comme ça
 *
 * si vous avez fini :
 *
 * Faites en sorte de faire fonctionner cette vue
 * en la scindant en plusieurs composants :
 *
 * - un composant mère, ce fichier
 * - un composant avec le champ de formulaire + le bouton de soumission pour ajouter une ligne
 * - un composant pour afficher la liste d'éléments
 *
 * Ce composant mère a les états, les autres composants ne sont que des composants de vue à qui on passe des props
 */

const TodoList = () => {
	const [list, setList] = useState(['salade', 'pomme', 'pizza'])
	const addItem = (item) => {
		setList([...list, item])
	}
	const deleteItem = (index) => {
		list.splice(index, 1)
		setList([...list])
	}
	return (
		<View>
			<Text>Modifier le code pour afficher une "todo list" ultra basique : un formulaire d'ajout d'éléments + l'affichage de la liste</Text>

			<AddForm onSubmit={addItem} />

			<List items={list} onDelete={deleteItem} />
		</View>
	);
}


const AddForm = ({ onSubmit }) => {
	const [inputValue, setInputValue] = useState('')
	return (
		<View>
			<TextInput
				style={{
					borderColor: "black",
					borderWidth: 1,
					marginVertical: 10,
					padding: 5
				}}
				onChangeText={(text) => {
					setInputValue(text)
				}}
				value={inputValue}
			/>

			<Button
				title="Ajouter un élément"
				onPress={() => {
					onSubmit(inputValue)
					setInputValue('')
				}}
			/>
		</View>
	)
}

const List = ({items, onDelete}) => {
	return (
		items.map((item, index) => (
			<View style={{ flexDirection: "row", marginVertical: 10, alignItems: "center"}}>
				<Text style={{ marginRight: 10 }}>{index}. {item}</Text>
				<Button title="Supprimer" onPress={() => onDelete(index) } />
			</View>
		))
	)
}


export default TodoList
