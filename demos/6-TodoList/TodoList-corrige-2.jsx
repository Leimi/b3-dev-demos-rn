import React, { useState } from 'react';
import { View, Text, Button, TextInput } from 'react-native';

/**
 * Exercice
 * 
 * Créer une vue qui va gérer :
 * 
 * - l'affichage d'une liste,
 * - un ajout d'éléments dans cette liste via un champ texte,
 * - la suppression d'élément de cette liste en cliquant sur un élément de la liste
 * 
 * pour l'instant, pas grave si on arrive pas à scroller correctement dans notre liste
 * une fois qu'elle est longue... on veut gérer une toute petite liste qui tient sur
 * l'écran comme ça
 * 
 * si vous avez fini :
 * 
 * Faites en sorte de faire fonctionner cette vue
 * en la scindant en plusieurs composants : 
 * 
 * - un composant mère, ce fichier
 * - un composant avec le champ de formulaire + le bouton de soumission pour ajouter une ligne
 * - un composant pour afficher la liste d'éléments
 * 
 * Ce composant mère a les états, les autres composants ne sont que des composants de vue à qui on passe des props
 */
const TodoList = () => {
	const [list, setList] = useState(['pates', 'huile d\'olive', 'PQ'])
	const onItemPress = (index) => {
		setList(previousList => {
			const newList = [...previousList]
			newList.splice(index, 1)
			return newList
		})
	}
	const onSubmit = (input) => {
		setList(previousList => {
			return [...previousList, input]
		})
	}
	return (
		<View>
			<Form onSubmit={onSubmit} />
			<List items={list} onItemPress={onItemPress} />
		</View>
	);
}

const List = ({ items, onItemPress }) => {
	return (
		<View>
			<Text
				style={{
					fontWeight: 'bold',
					marginTop: 12,
					fontSize: 24,
					marginBottom: 12
				}}
				accessibilityRole="header"
			>
				Liste
			</Text>

			{items.map((item, index) =>
				<Text
					onPress={() => {
						onItemPress(index)
					}}
					key={`${index}${item}`}
				>
					• {item}
				</Text>
			)}
		</View>
	)
}

const Form = ({ onSubmit }) => {
	const [input, setInput] = useState('')
	return (
		<>
			<Text>Aliment à ajouter :</Text>
			<TextInput
				accessibilityLabel="Aliment à ajouter"
				value={input}
				style={{
					borderColor: "#ddd",
					borderWidth: 1,
					borderRadius: 4,
					paddingHorizontal: 12,
					paddingVertical: 3,
					marginTop: 6,
					marginBottom: 12
				}}
				onChangeText={text => setInput(text)}
			/>
			<Button
				onPress={() => {
					onSubmit(input)
				}}
				title="Ajouter"
			/>
		</>
	)
}

export default TodoList
