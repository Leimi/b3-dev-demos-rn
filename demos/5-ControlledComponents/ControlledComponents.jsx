import React, { useState } from 'react';
import { View, Text, TextInput, Switch, Button, Alert} from 'react-native';

const StatefulComponent = (props) => {
	const [isSwitchEnabled, setSwitchEnabled] = useState(false);
	const [input, setInput] = useState('');
	const toggleSwitch = () => {
		console.log(`toggleSwitch, appel setSwitchEnabled(${!isSwitchEnabled})`);
		setSwitchEnabled(previousState => !previousState);
	}

	const userHasGreetedMe = /bonjour|salut|hello/.test(input)

	console.log('nouveau rendu', {isSwitchEnabled, input, userHasGreetedMe});
	return (
		<>
			<View style={{
				flexDirection: 'row'
			}}>
				<Text style={{flex: 1}}>
					Ce composant a un état : à l'interaction utilisateur, l'UI va changer au cours du temps
				</Text>
				<Switch
					onValueChange={toggleSwitch}
					value={isSwitchEnabled}
				/>
			</View>
			<Text style={{
				margin: 24,
				fontSize: 24,
				fontWeight: 'bold',
				textAlign: 'center'
			}}>
				{ isSwitchEnabled
					? 'Le bouton est coché !'
					: 'Le bouton n\'est pas coché !'
				}
			</Text>

			<Text>Champ de test :</Text>
			<TextInput
				onChangeText={text => {
					console.log(`onChangeText, appel setInput("${text}")`)
					setInput(text)
				}}
				value={input}
				style={{
					borderColor: "#ddd",
					borderWidth: 1,
					borderRadius: 4,
					paddingHorizontal: 12,
					paddingVertical: 3,
					marginTop: 6,
					marginBottom: 12
				}}
			/>

			{ userHasGreetedMe &&
				<Text style={{marginBottom: 12}}>Hey, bonjour à vous aussi !</Text>
			}

			<Text>Magie : on peut aussi changer l'état d'un élément d'UI sans passer directement par lui.</Text>

			<View style={{marginTop: 24}}>
				<Button
					onPress={toggleSwitch}
					title="Abracadabra !"
				/>
			</View>
		</>
	);
}

export default StatefulComponent
