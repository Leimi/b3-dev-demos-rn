import React, { useState, useEffect } from 'react';
import { ScrollView, View, Text, TextInput, Image, Button } from 'react-native';

const ComponentWithEffect = (props) => {
	const [image, setImage] = useState({})
	const [input, setInput] = useState('')
	useEffect(() => {
		console.log('useEffect avec `input` en dépendance');
		if (!/rick|morty|summer|beth|jerry/i.test(input)) {
			setImage({})
			return;
		}
		console.log('useEffect : appel API distante');
		fetch(`https://rickandmortyapi.com/api/character/?name=${input}&status=alive`)
			.then(response => response.json())
			.then(data => {
				if (!data.results || !data.results[0]) {
					return;
				}
				console.log('useEffect : appel setImage');
				Image.getSize(data.results[0].image, (width, height) => {
					setImage({
						uri: data.results[0].image,
						width,
						height
					})
				})
			})
	}, [input])

	console.log('nouveau rendu', { image, input });

	return (
		<View style={{flex: 1}}>
			<Text style={{ marginBottom: 12 }}>
				Ici nous allons voir comment avoir du code générant des "effets de bord" dans un composant React.
			</Text>
			<Text style={{ marginBottom: 12 }}>
				On peut décrire du code à lancer uniquement au 1er affichage du composant, ou à relancer uniquement au changement de certaines variables du composant, via le hook <Text style={{ fontWeight: 'bold' }}>useEffect</Text>
			</Text>

			<Text>Afficher un personnage de Rick &amp; Morty :</Text>
			<TextInput
				onChangeText={text => {
					console.log('onChangeText : appel setInput');
					setInput(text)
				}}
				value={input}
				style={{
					borderColor: "#ddd",
					borderWidth: 1,
					borderRadius: 4,
					height: 40,
					paddingHorizontal: 12,
					paddingVertical: 3,
					marginTop: 6,
					marginBottom: 12
				}}
			/>

			{ !!image.uri &&
				<Image
					source={{uri: image.uri}}
					style={{
						width: image.width,
						height: image.height
					}}
				/>
			}
		</View>
	);
}

export default ComponentWithEffect
