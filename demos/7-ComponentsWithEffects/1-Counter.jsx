import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Image, Button } from 'react-native';

const Counter = (props) => {
	const [seconds, setSeconds] = useState(0)
	useEffect(() => {
		console.log('useEffect avec tableau vide en dépendance');
		const interval = setInterval(() => {
			/**
			 * attention : useEffect est plein de petites subtilités…
			 * par exemple, il faut comprendre qu'il a comme variables locales
			 * les valeurs du composant *au moment où le useEffect a été appelé*.
			 * Ici, useEffect a été appelé au 1er rendu composant : `seconds` vaut donc tjrs 0 !
			 *
			 * D'où l'intérêt d'utiliser un callback pour setSeconds, sinon, ça ne marche pas !
			 */
			setSeconds(previousState => {
				console.log(`tick interval, appel setSeconds(${previousState + 1}). \`seconds = ${seconds}\``);
				return previousState + 1
			})
		}, 1000);
		return () => {
			console.log('nettoyage du useEffect (clearInterval)');
			clearInterval(interval)
		}
	}, [])

	console.log('nouveau rendu', { seconds });

	return (
		<View>
			<Text style={{ marginBottom: 12 }}>
				Ici nous allons voir comment avoir du code générant des "effets de bord" dans un composant React.
			</Text>
			<Text style={{ marginBottom: 12 }}>
				On peut décrire du code à lancer uniquement au 1er affichage du composant, ou à relancer uniquement au changement de certaines variables du composant, via le hook <Text style={{ fontWeight: 'bold' }}>useEffect</Text>
			</Text>

			<Text style={{ marginBottom: 12 }}>
				Cela fait {seconds} seconde{seconds > 1 && 's'} que vous avez ouvert cet écran.
			</Text>
		</View>
	);
}

export default Counter
