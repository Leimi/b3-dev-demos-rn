import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const SimpleComponent = (props) => {
	return (
		<View>
			<Text
				style={styles.header}
				accessibilityRole="header"
			>
				{props.children}
			</Text>

			<Text style={styles.text}>
				Ceci est un <Text style={styles.bold}>paragraphe</Text>. Il peut passer sur plusieurs lignes tout seul si jamais il a besoin, évidemment.
			</Text>

			<Text style={styles.text}>
				Ceci est un autre paragraphe, <Text style={styles.bold}>plus court</Text>.
			</Text>

			<Text
				style={styles.subheader}
				accessibilityRole="header"
			>
				Exemples d'expressions JS (voir le code)
			</Text>

			<Text style={styles.text}>
				Vous saviez que 2 + 2 = { 2 + 2 } ?
			</Text>

			<Text style={styles.text}>
				Mais aussi que 2 + 2 = {
				"je teste quelque chose" === "de faux"
					? 5
					: 4
				} ?
			</Text>

			<Text style={styles.text}>
				Et aussi que 2 + 2 = { "ceci est truethy, retourne donc ce qui est après le &&" && 4 } ?!
			</Text>

			<Text
				style={styles.subheader}
				accessibilityRole="header"
			>
				Date du prochain cours
			</Text>

			<View style={{alignItems: 'center'}}>
				<Text style={styles.text}>Rendez-vous vendredi chez : </Text>
				<Image
					source={require('../../assets/logo-mds.jpg')}
					accessibilityLabel="My Digital School"
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	header: {
		fontWeight: 'bold',
		fontSize: 24,
		marginBottom: 12
	},
	subheader: {
		fontWeight: 'bold',
		fontSize: 20,
		marginVertical: 12
	},
	text: {
		fontSize: 16,
		lineHeight: 24,
		marginBottom: 12
	},
	bold: {
		fontWeight: 'bold'
	}
})

export default SimpleComponent
