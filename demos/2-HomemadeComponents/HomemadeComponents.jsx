import React from 'react';
import { Text } from 'react-native';

/**
 * Exercice
 *
 * créer dans le dossier "components" déjà créé,
 * des composants React réutilisables tirés de la vue de "SimpleComponents.jsx" :
 *
 * - un composant Heading pour les titres
 * - un composant Paragraph pour les ... paragraphes !
 * - un composant Bold pour les parties de texte en gras
 * - pas forcément la peine de créer un composant réutilisable de la partie "rdv vendredi" avec l'image… vous pouvez juste recopier le code
 *
 * puis importer ces composants ici et les utiliser afin de recréer une vue
 * similaire à "SimpleComponents.jsx"
 */
const HomemadeComponents = () => {
	return (
		<Text>Modifier le code pour que le rendu de cette démo soit le même que la démo 1, en créant des composants réutilisables</Text>
	);
}


export default HomemadeComponents
