import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, View, FlatList, Text } from 'react-native';
import * as Linking from 'expo-linking'

/**
 * Exercice : créer une vue qui va afficher une liste des dernières news du site MyDigitalSchool.
 *
 * La liste de news est disponible au format JSON sur https://mds-api.netlify.app/news.json.
 *
 * Réfléchir à comment récupérer ces données,
 * puis comment, pour chacune des news :
 *
 * - afficher son titre
 * - afficher son extrait texte (champ "excerpt")
 * - permettre à l'utilisateur de cliquer sur le titre de la news pour ouvrir le navigateur web de son téléphone sur la page de la news (champ "url").
 * 		- voir le composant TouchableOpacity pour avoir un bouton "invisible" en tant que titre,
 * 		- voir "expo-linking" (l'installer) pour ouvrir le navigateur : https://docs.expo.io/versions/v41.0.0/sdk/linking/.
 *
 * Prendre le temps de faire une liste avec un design un minimum compréhensible (par exemple, titres écrits plus gros, en gras, éléments de la liste bien séparés, etc. Faites comme vous voulez)
 *
 * Utiliser le composant FlatList pour afficher la liste de news : https://reactnative.dev/docs/flatlist
 *
 * Si vous avez fini tout ça, améliorez la liste pour afficher, pour chaque news, son image à côté du titre et du texte.
 */
const MDSNews = () => {
	const [newsItems, setNewsItems] = useState([])
	useEffect(() => {
		fetch("https://mds-api.netlify.app/news.json")
			.then(response => response.json())
			.then(json => {
				setNewsItems(json)
			})
	}, [])

	return (
		<FlatList data={newsItems} renderItem={({ item }) => (
			<View style={{
				marginBottom: 10,
				flexDirection: "row"
			}}>
				<Image
					style={{
						width: 100,
						height: 100
					}}
					source={{ uri: item.image }} 
				/>
				<View style={{ marginLeft: 10, flex: 1 }}>
					<TouchableOpacity onPress={() => {
						Linking.openURL(item.url)
					}}>
						<Text style={{ fontSize: 18, fontWeight: "bold"}}>
							{item.title}
						</Text>
					</TouchableOpacity>
					<Text>
						{item.excerpt}
					</Text>
				</View>
			</View>
		)}/>
	)
}


export default MDSNews
